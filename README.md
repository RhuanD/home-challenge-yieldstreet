**Project Name: YieldStreet Cypress Challenge**

---

**Overview:**
This project contains automated tests written in Cypress for the technical challenge provided by YieldStreet. The purpose of these tests is to validate the functionality and behavior of the YieldStreet application.

---

**Getting Started:**
1. Clone this repository to your local machine.
2. Install Cypress using npm:
    ```
    npm install cypress --save-dev
    ```
3. Install mochawesome using npm:
    ```
    npm install --save-dev mochawesome
    ``` 
4. Once Cypress is installed, navigate to the project directory:
    ```
    npx cypress open
    ```
    or you can utilize this command.
    ```
    npx cypress run
    ```
    using this command, you have all the generated reports on `cypress/reports`.
    In case you don't want to use the cypress run, you can jump to step 4.
5. Select the desired spec file to run the tests.

---

**Folder Structure:**
- `cypress/e2e/[system]`: Contains the test files written in Cypress.
- `cypress/support`: Contains custom commands and support files.
- `cypress/fixtures`: Contains fixtures for test data, if applicable.

---

**License:**
This project belongs to [Rhuan Dornelles](https://www.linkedin.com/in/rhuan-dornelles/).

---

**Acknowledgements:**
I would like to thank YieldStreet for providing the opportunity to work on this challenge.

---
