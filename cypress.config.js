const { defineConfig } = require("cypress");

module.exports = defineConfig({
  reporter: 'mochawesome',
  reporterOptions: {
    screenshotOnRunFailure: true,
    reportDir: 'cypress/reports/mochawesome', 
    overwrite: true, 
    html: true, 
    json: true,
    reportFilename: 'report',
    timestamp: 'mmddyyyy_HHMMss',
    cdn: true,
    charts: true
  },
  screenshotsFolder: 'cypress/reports/mochawesome/assets',
  e2e: {
    chromeWebSecurity: false,
    setupNodeEvents(on, config) {
      
      // implement node event listeners here
    },
  },
});
