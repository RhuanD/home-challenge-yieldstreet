/// <reference types="Cypress"/>

describe('Get an ability', () => {
    it('Verify that the API returns correct details for a specific ability', () => {
        cy.request('GET', 'https://pokeapi.co/api/v2/ability/66')
        .then((response) => {
            expect(response.status).to.eq(200)
            assert.strictEqual(response.body.generation.name, 'blaze', 'Ability name should be "blaze"')
            assert.isNotEmpty(response.body.names, 'Abilities array should not be empty')
        })
    })
  })