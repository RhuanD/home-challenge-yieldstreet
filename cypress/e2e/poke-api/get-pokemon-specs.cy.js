/// <reference types="Cypress"/>

describe('Get a pokémon', () => {
    it('Verify that the API returns correct details for a specific Pokémon.', () => {
        cy.request('GET', 'https://pokeapi.co/api/v2/pokemon/bulbasaur')
        .then((response) => {
            expect(response.status).to.eq(200)
            assert.strictEqual(response.body.name, 'bulbasaur', 'Pokemon name should be "bulbasaur"')
            assert.isNotEmpty(response.body.abilities, 'Abilities array should not be empty')
            const isGrassType = response.body.types.some((type) => type.type.name === 'grass')
            assert.isTrue(isGrassType, 'Pokemon should be of type "grass"')
        })
    })
    it('Validate that the types returned for a Pokémon are valid.', () => {
        cy.request('GET', 'https://pokeapi.co/api/v2/pokemon/charizard')
        .then((response) => {
            expect(response.status).to.eq(200)
            assert.strictEqual(response.body.name, 'charizard', 'Pokemon name should be "charizard"')
            assert.isNotEmpty(response.body.abilities, 'Abilities array should not be empty')
            const isGrassType = response.body.types.some((type) => type.type.name === 'fire')
            assert.isTrue(isGrassType, 'Pokemon should be of type "fire"')
        })
    })
    it('Verify Error Response for Invalid Pokémon.', () => {
        cy.request({
            method: 'GET',
            url: 'https://pokeapi.co/api/v2/pokemon/charizardop',
            failOnStatusCode: false})
        .then((response) => {
            expect(response.status).to.eq(404)
        })
    })
  })