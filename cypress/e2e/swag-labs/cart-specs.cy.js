/// <reference types="Cypress"/>

describe('Cart management test of Swag Labs', () => {
    beforeEach(() => {
      cy.visit('https://www.saucedemo.com/')
      cy.get('#user-name').type('standard_user')
      cy.get('#password').type('secret_sauce')
      cy.get('#login-button').click()
      cy.get('#add-to-cart-sauce-labs-bike-light').click()
      cy.get('.shopping_cart_link').click()
    })
    it('Verify that users can view their cart contents.', () => {
      cy.get('.title').invoke('text').should('be.equals', 'Your Cart')
      cy.get('.inventory_item_name').invoke('text').should('be.equal', 'Sauce Labs Bike Light')
      cy.screenshot('cart-added')
    }),
    it('Verify that users can remove products from the cart.', () => {
      cy.get('#remove-sauce-labs-bike-light').click()
      cy.get('.inventory_item_name').should('not.exist')
      cy.screenshot('cart-removed')
    })
  })