/// <reference types="Cypress"/>

describe('Checkout test of Swag Labs', () => {
    beforeEach(() => {
      cy.visit('https://www.saucedemo.com/')
      cy.get('#user-name').type('standard_user')
      cy.get('#password').type('secret_sauce')
      cy.get('#login-button').click()
      cy.get('#add-to-cart-sauce-labs-bike-light').click()
      cy.get('.shopping_cart_link').click()
    })
    it('Verify that users can proceed through the checkout process smoothly.', () => {
        cy.get('#checkout').click()
        cy.get('.title').invoke('text').should('be.equal', 'Checkout: Your Information')
        cy.screenshot('proceed-checkout')
    }),
    it('Verify that users can enter shipping and payment information.', () => {
        cy.get('#checkout').click()
        cy.get('#first-name').type('Rhuan')
        cy.get('#last-name').type('Dornelles')
        cy.get('#postal-code').type('88132860')
        cy.screenshot('added-info')
        cy.get('#continue').click()
        cy.get('.summary_info').invoke('text').should('contain', 'Payment Information')
        cy.screenshot('payment-info')
    }),
    it('Verify that users receive confirmation after completing the checkout.', () => {
        cy.get('#checkout').click()
        cy.get('#first-name').type('Rhuan')
        cy.get('#last-name').type('Dornelles')
        cy.get('#postal-code').type('88132860')
        cy.get('#continue').click()
        cy.get('#finish').click()
        cy.get('.complete-header').invoke('text').should('be.equal', 'Thank you for your order!')
        cy.screenshot('success-info')
    })
    it('Verify that users can enter shipping and payment information.', () => {
        cy.get('#remove-sauce-labs-bike-light').click()
        cy.screenshot('removed-item')
        cy.get('.inventory_item_name').should('not.exist')
        cy.get('#checkout').click()
        cy.screenshot('clicked-checkout-without-product')
        cy.get('#first-name').type('Rhuan')
        cy.get('#last-name').type('Dornelles')
        cy.get('#postal-code').type('88132860')
        cy.get('#continue').click()
        cy.get('#finish').click()
        cy.get('.complete-header').should('not.exist')
        cy.screenshot('should-have-failed')
    })
  })