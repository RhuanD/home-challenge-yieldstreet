/// <reference types="Cypress"/>

describe('Login test of Swag Labs', () => {
  beforeEach(() => {
    cy.visit('https://www.saucedemo.com/')
  })
  it('Verify that users can login with valid credentials.', () => {
    cy.get('#user-name').type('standard_user')
    cy.get('#password').type('secret_sauce')
    cy.get('#login-button').click()
    cy.get('.title').should('exist').invoke('text').should('be.equal', 'Products')
    cy.screenshot('success-login')
  }),
  it('Verify that users cannot login with invalid credentials.', () => {
    cy.get('#user-name').type('standard_user')
    cy.get('#password').type('secret_sauce1')
    cy.get('#login-button').click()
    cy.get('[data-test="error"]').should('exist').invoke('text').should('contain', 'Epic sadface')
    cy.screenshot('failure-login')
  })
})