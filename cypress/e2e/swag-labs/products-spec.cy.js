/// <reference types="Cypress"/>

describe('Products test of Swag Labs', () => {
    beforeEach(() => {
      cy.visit('https://www.saucedemo.com/')
      cy.get('#user-name').type('standard_user')
      cy.get('#password').type('secret_sauce')
      cy.get('#login-button').click()
    })
    it('Verify that products are displayed correctly.', () => {
      cy.get('.title').should('exist').invoke('text').should('be.equal', 'Products')
      cy.screenshot('listed-products')
    }),
    it('Verify that users can add products to cart from home page.', () => {
      cy.get('#add-to-cart-sauce-labs-bike-light').click()
      cy.get('.shopping_cart_link').click()
      cy.screenshot('click-home-page')
      cy.get('.title').invoke('text').should('be.equals', 'Your Cart')
      cy.get('.inventory_item_name').invoke('text').should('be.equal', 'Sauce Labs Bike Light')
      cy.screenshot('added-to-cart')
    }),
    it('Verify that users can add products to the cart from product page.', () => {
      cy.get('.inventory_item_name').contains('Sauce Labs Bolt T-Shirt').click()
      cy.get('#add-to-cart-sauce-labs-bolt-t-shirt').click()
      cy.screenshot('click-product-page')
      cy.get('.shopping_cart_link').click()
      cy.get('.inventory_item_name').invoke('text').should('be.equal', 'Sauce Labs Bolt T-Shirt')
      cy.screenshot('added-to-cart')
    })
  })