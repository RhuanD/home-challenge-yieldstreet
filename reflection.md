# Approach
- On Poke API, I had a conservative approach because the system itself is just too big, so I made some smoke tests and automated the core of the application, since it's what people mostly use on the API.
- On Swag Labs, I took a more aggressive approach, because the system itself is not that big, its features are simple, what gave me time even to find some bugs and do some kind of regressive test.

# Notable challenges
- On Poke API the most notable challenge was to find a way to find the core features of the system, but not make the same test over and over again.
- On Swag Labs, I struggled a little with the implementation of the mochawesome reports, since it was my first time implementing that lib in specific.

# My assessment of the testability of the systems
- On both systems, even though I took a different approach, they were easy to understand, with an easy and intuitive experience. Overall, they were smooth and clean to use.

# What I would've done different
- I think I would have apply more time on the smoke tests of the Swag Labs. Some bugs I found wouldn't have happened if there was more time invested on the system.

# My assessment of the quality of my test suite
- Overall, I believe my test suite performed adequately. I conducted various failure tests, effectively covered a significant portion of the Swag Labs system, and addressed most of the essential aspects of the Poke API. Given the time I received, I found the outcomes satisfactory. This experience provided valuable insights and expanded my knowledge. I look forward to utilizing this newfound knowledge alongside future learnings. Once again, I thank Yieldstreet for the oportunity.